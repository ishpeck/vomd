/* Copyright (C) 2016 Anthony "Ishpeck" Tedjamulia
   See LICENSE for licensing information. */
#include "assets.h"

static void abort_due_to_failed_alloc(void) {
    fprintf(stderr, "Could not allocate memory!\n"
            "Make sure that you aren't trying to serve "
            "out more data than you have RAM.\n");
    exit(1);
}

servableAsset_t new_asset(char *mime, char *url) {
    servableAsset_t newAsset;
    newAsset.mimetype=mime;
    newAsset.endpoint=url;
    newAsset.content=NULL;
    newAsset.size=0;
    return newAsset;
}

servableDataBlock_t new_data_block(off_t totalSize) {
    servableDataBlock_t dataBlock;
    dataBlock.assets=NULL;
    dataBlock.assetCount=0;
    dataBlock.data=calloc(totalSize, sizeof(char));
    if(!dataBlock.data)
        abort_due_to_failed_alloc();
    dataBlock.size=totalSize;
    return dataBlock;
}

void destroy_data_block(servableDataBlock_t destroyMe) {
    if(destroyMe.assets)
        free(destroyMe.assets);
    if(destroyMe.data)
        free(destroyMe.data);
    destroyMe.size=0;
    destroyMe.assetCount=0;
}

servableAsset_t *lookup_asset(servableDataBlock_t *data, const char *endpoint) {
    unsigned long upperBound=data->assetCount;
    unsigned long lowerBound=0;
    unsigned long pivotPoint=1;
    int comparisonResult;
    while(lowerBound<upperBound && upperBound-lowerBound>1) {
        pivotPoint=lowerBound+(upperBound-lowerBound)/2;
        comparisonResult=strcmp(data->assets[pivotPoint].endpoint, endpoint);
        if(comparisonResult==0)
            return &data->assets[pivotPoint];
        if(RIGHT_ASSET_COMES_AFTER_LEFT(comparisonResult))
            lowerBound=pivotPoint;
        else
            upperBound=pivotPoint;
    }
    for(;lowerBound<=upperBound;++lowerBound)
        if(strcmp(data->assets[lowerBound].endpoint, endpoint)==0)
            return &data->assets[lowerBound];
    return NULL;
}
