/* Copyright (C) 2016 Anthony "Ishpeck" Tedjamulia
   See LICENSE for licensing information. */
#ifndef _VOMD_MAIN_HEADER_
#define _VOMD_MAIN_HEADER_

#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <sys/stat.h>

#include "assets.h"
#include "config.h"
#include "server.h"

#include "platform.h"

#define NO_MORE_ARGS -1
#define DEFAULT_LISTENING_PORT 80

#define VOMD_VERSION_IDENTIFIER "a3-2016-07-16"

#endif
