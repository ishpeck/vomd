/* Copyright (C) 2016 Anthony "Ishpeck" Tedjamulia
   See LICENSE for licensing information. */
#include "http.h"

static int is_line_feed(char input) {
    return input=='\r';
}

static http_response generate_response(const char *protocol,
                                       const char *statusLine,
                                       const char *mimetype,
                                       const char *message,
                                       size_t len,
                                       int shallDisconnect) {
    http_response response;
    size_t maxDateLen=60;
    size_t maxResponseSize=len+strlen(protocol)+
        strlen(statusLine)+strlen(mimetype)+
        maxDateLen+RESPONSE_BUFFER_PADDING;
    size_t headersLen;
    char *dateBuffer=calloc(maxDateLen, sizeof(char));
    char *responseBody=calloc(maxResponseSize, sizeof(char));
    char *contentBegin=NULL;
    time_t clock=time(NULL);
    struct tm theTime;
    gmtime_r(&clock, &theTime);
    strftime(dateBuffer, maxDateLen, "Date: %a, %B %d %H:%M:%S %Z %G", &theTime);

    snprintf(responseBody,
             maxResponseSize,
             "%s %s\r\n"
             "%s\r\nServer: vomd\r\n"
             "Content-Type: %s\r\n"
             "Content-Length: %d\r\n"
             "\r\n",
             protocol, statusLine, dateBuffer, mimetype, len);

    headersLen=strlen(responseBody);
    contentBegin=responseBody+headersLen;
    memcpy(contentBegin, message, len);

    free(dateBuffer);

    response.responseBody=responseBody;
    response.responseLength=len+headersLen+2;
    response.disconnectAfterTx=shallDisconnect;
    return response;
}

static http_response respond_413v11(void) {
    char msg[]="Requests must be smaller than "MAXIMUM_REQUEST_BODY_WARN" bytes.";
    return generate_response("HTTP/1.1",
                             "413 Request Entity Too Large",
                             "text/plain",
                             msg,
                             strlen(msg),
                             DISCONNECT_AFTER_TRANSMISSION);
}

static http_response respond_501v11(int shallDisconnect) {
    char msg[]="This server only accepts GET requests.";
    return generate_response("HTTP/1.1",
                             "501 Not Implemented",
                             "text/plain",
                             msg,
                             strlen(msg),
                             shallDisconnect);
}

static http_response respond_501v10(int shallDisconnect) {
    char msg[]="This server only accepts GET requests.";
    return generate_response("HTTP/1.0",
                             "501 Not Implemented",
                             "text/plain",
                             msg,
                             strlen(msg),
                             shallDisconnect);
}

static http_response respond_500v11(void) {
    char msg[]="Something went wrong in the web server.";
    return generate_response("HTTP/1.1",
                             "500 Internal Error",
                             "text/plain",
                             msg,
                             strlen(msg),
                             REMAIN_CONNECTED_AFTER_TRANSMISSION);
}

static http_response respond_500v10(void) {
    char msg[]="Something went wrong in the web server.";
    return generate_response("HTTP/1.0",
                             "500 Internal Error",
                             "text/plain",
                             msg,
                             strlen(msg),
                             DISCONNECT_AFTER_TRANSMISSION);
}

static http_response respond_505v10(void) {
    char msg[]="HTTP protocol not supported.  Please use HTTPv1.0 or HTTPv1.1";
    return generate_response("HTTP/1.0",
                             "505 Protocol not supported",
                             "text/plain",
                             msg,
                             strlen(msg),
                             DISCONNECT_AFTER_TRANSMISSION);
}

static http_response process_get_request(servableDataBlock_t *src,
                                         const char *protocol,
                                         const char *endpoint,
                                         int shallDisconnect) {
    servableAsset_t *asset=lookup_asset(src, endpoint);
    int responseLength=MAXIMUM_ENDPOINT_LENGTH+12;
    char msg[responseLength];
    snprintf(msg, responseLength, "%s not found", endpoint);
    
    if(!asset)
        return generate_response(protocol,
                                 "404 Not Found",
                                 "text/plain",
                                 msg,
                                 strlen(msg),
                                 shallDisconnect);
    return generate_response(protocol,
                             "200 OK",
                             asset->mimetype,
                             asset->content,
                             asset->size,
                             shallDisconnect);
}

static int is_white_space(const char inp) {
    return inp==' '|| inp=='\t' || inp=='\r' || inp=='\n' || inp=='\v';
}

static char *nextColumn(char *position) {
    while(!is_white_space(*position)) ++position;
    while(is_white_space(*position)) ++position;
    return position;
}

static int cpy_column(char *dest,
                      const char *src,
                      unsigned int ultMax,
                      unsigned int destMax,
                      unsigned int srcOffset) {
    int i;
    for(i=0;i+srcOffset<ultMax && i<destMax && !is_white_space(src[i+srcOffset]);++i)
        dest[i]=src[i+srcOffset];
    return i+srcOffset;
}

static int first_non_white_space(const char *src, unsigned int max, int i) {
    for(;i<max && is_white_space(src[i]);++i);
    return i;
}

static http_response process_httpv11(servableDataBlock_t *src,
                                     const char *method,
                                     const char *url,
                                     int shallDisconnect) {
    if(strcmp(method, "GET")==0)
        return process_get_request(src, "HTTP/1.1", url, shallDisconnect);
    return respond_501v11(shallDisconnect);
}

static http_response process_httpv10(servableDataBlock_t *src,
                                     const char *method,
                                     const char *url,
                                     int shallDisconnect) {
    if(strcmp(method, "GET")==0)
        return process_get_request(src, "HTTP/1.0", url, shallDisconnect);
    return respond_501v10(shallDisconnect);
}

static request_processing determine_proc(const char *httpVer) {
    return (strcmp(httpVer, "HTTP/1.1")==0) 
        ? process_httpv11 
        : process_httpv10;
}

http_response process_http_request(servableDataBlock_t *src,
                                   const char *requestBody,
                                   unsigned int requestLength) {
    char httpMethod[MAX_HTTP_METHOD_LENGTH];
    char reqUrl[MAX_ENDPOINT_BUFFER_LENGTH];
    char httpVersion[MAX_HTTP_VERSION_LENGTH];
    int i;

    if(requestLength>MAXIMUM_REQUEST_BODY_BYTES)
        return respond_413v11();

    memset(httpMethod, 0, MAX_HTTP_METHOD_LENGTH);
    memset(reqUrl, 0, MAX_ENDPOINT_BUFFER_LENGTH);
    memset(httpVersion, 0, MAX_HTTP_VERSION_LENGTH);

    i=cpy_column(httpMethod,
                 requestBody,
                 requestLength, 
                 MAX_HTTP_METHOD_LENGTH,
                 0);
    i=cpy_column(reqUrl,
                 requestBody,
                 requestLength, 
                 MAX_ENDPOINT_BUFFER_LENGTH,
                 first_non_white_space(requestBody, requestLength, i));
    i=cpy_column(httpVersion,
                 requestBody,
                 requestLength, 
                 MAX_HTTP_VERSION_LENGTH,
                 first_non_white_space(requestBody, requestLength, i));

    if( (strcmp(httpVersion, "HTTP/1.1")!=0) &&
        (strcmp(httpVersion, "HTTP/1.0")!=0) &&
        (strcmp(httpVersion, "")!=0))
        return respond_505v10();

    return determine_proc(httpVersion)(src, httpMethod, reqUrl,
        strcasestr(requestBody, "Connection: keep-alive")==NULL);
}
