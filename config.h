/* Copyright (C) 2016 Anthony "Ishpeck" Tedjamulia
   See LICENSE for licensing information. */
#ifndef _yon_config_parser_header_file_
#define _yon_config_parser_header_file_

#include <sys/stat.h>

#include "assets.h"
#include "buffer.h"

typedef struct {
    char *filePath;
    char *mimetype;
    char *endpoint;
    void *next;
} assetListCell_t;

servableDataBlock_t parse_asset_source(FILE *src);

#define CONTINUE_READING_CONFIG 1
#define DONE_READING_CONFIG 0

#endif

