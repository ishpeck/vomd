/* Copyright (C) 2016 Anthony "Ishpeck" Tedjamulia
   See LICENSE for licensing information. */
#include "config.h"

static int is_whitespace(const int input) {
    return input==' ' || input=='\n' || input=='\t' || input=='\v' || input=='\r';
}

static int is_useful_info(const int input) {
    return input!=EOF && !is_whitespace(input);
}

static int insert_char(buffer_t *buf, const int input) {
    if(!is_useful_info(input))
        return input;
    buffer_push_char(buf, (char)input);
    return input;
}

static void abort_due_to_bad_file(const char *filePath) {
    fprintf(stderr, "Couldn't find file: %s\n", filePath);
    exit(1);
}

static void abort_due_to_no_mimetype(void) {
    fprintf(stderr, "Must spceify a MIME type\n");
    exit(2);
}

static void abort_due_to_no_endpoint(void) {
    fprintf(stderr, "Must specify an endpoint!\n");
    exit(3);
}

static void abort_due_to_overweight_endpoint(const char *endpoint) {
    fprintf(stderr, "The given endpoint is too long: %s", endpoint);
    exit(5);
}

static void abort_due_to_duplicate_endpoints(const char *endPoint) {
    fprintf(stderr, "The endpoint \"%s\" appears more than once\n", endPoint);
    exit(4);
}

static void dispose(void *disposable) {
    if(disposable)
        free(disposable);
}

static void cleanup_asset_list(assetListCell_t *cur) {
    assetListCell_t *next;
    while(cur) {
        next=cur->next;
        dispose(cur->mimetype);
        dispose(cur->endpoint);
        dispose(cur->filePath);
        dispose(cur);
        cur=next;
    }
}

static int get_config_column(FILE *src, buffer_t *buf) {
    int input;
    buffer_reset(buf);
    while(is_whitespace(input=fgetc(src)));
    while(is_useful_info(insert_char(buf, input)))
        input=fgetc(src);
    return input;
}


static assetListCell_t *read_config_line(FILE *src, buffer_t *buf) {
    assetListCell_t *cur;
    int lastConsumedChar=get_config_column(src, buf);
    if(buf->len<1)
        return NULL;
    cur=calloc(1, sizeof(assetListCell_t));
    if(lastConsumedChar==EOF)
        abort_due_to_no_mimetype();
    cur->filePath=buf_to_str(buf);

    lastConsumedChar=get_config_column(src, buf);
    if(buf->len<1)
        abort_due_to_no_mimetype();
    if(lastConsumedChar==EOF) 
        abort_due_to_no_endpoint();
    cur->mimetype=buf_to_str(buf);

    lastConsumedChar=get_config_column(src, buf);
    if(buf->len<1)
        abort_due_to_no_endpoint();
    if(buf->len>MAXIMUM_ENDPOINT_LENGTH)
        abort_due_to_overweight_endpoint(buffer_data(buf));
    cur->endpoint=buf_to_str(buf);
    return cur;
}

static int endpoint_after(assetListCell_t *one, assetListCell_t *two) {
    int comparisonResult;
    if(!one)
        return 0;
    comparisonResult=strcmp(one->endpoint, two->endpoint);
    if(comparisonResult==0)
        abort_due_to_duplicate_endpoints(two->endpoint);
    return RIGHT_ASSET_COMES_AFTER_LEFT(comparisonResult); 
}

static int insert_new_asset(assetListCell_t *prev, assetListCell_t *newest) {
    assetListCell_t *cur=prev->next;
    if(!newest)
        return DONE_READING_CONFIG;
    while(endpoint_after(cur, newest)) {
        prev=cur;
        cur=cur->next;
    }
    newest->next=cur;
    prev->next=newest;
    return CONTINUE_READING_CONFIG;
}

static assetListCell_t *stream_in_config_info(FILE *src, buffer_t *buf) {
    assetListCell_t *head=calloc(1, sizeof(assetListCell_t));
    while(insert_new_asset(head, read_config_line(src, buf)));
    fclose(src);
    return head;
}

static off_t add_string_size(const char *theString) {
    off_t size=strlen(theString);
    if(size<1)
        return size;
    return size+1;
}

static off_t add_file_size(const char *filePath) {
    struct stat statInfo;
    if(stat(filePath, &statInfo)<0) 
        abort_due_to_bad_file(filePath);
    return statInfo.st_size;
}

static off_t determine_block_size(assetListCell_t *cur) {
    off_t size=0;
    while(cur) {
        size+=add_string_size(cur->mimetype);
        size+=add_string_size(cur->endpoint);
        size+=add_file_size(cur->filePath);
        cur=cur->next;
    }
    return size;
}

static unsigned long count_assets(assetListCell_t *cur) {
    unsigned long sum=0;
    while(cur) {
        ++sum;
        cur=cur->next;
    }
    return sum;
}

static size_t capture_str(void *cursor, const char *str) {
    size_t len=strlen(str)+1;
    memcpy(cursor, str, len);
    return len;
}

static size_t capture_file(void *cursor, const char *filePath) {
    FILE *assetFile;
    size_t len, gotten;
    struct stat fileInfo;
    stat(filePath, &fileInfo);
    len=fileInfo.st_size;
    assetFile=fopen(filePath, "r");
    if((gotten=fread(cursor, sizeof(char), len, assetFile))!=len) {
        fprintf(stderr, "Expected %d bytes and got %d\n", len, gotten);
        abort_due_to_bad_file(filePath);
    }
    if(assetFile!=stdin)
        fclose(assetFile);
    return len;
}

static servableDataBlock_t initialize_block(assetListCell_t *cur) {
    void *cursor=NULL;
    unsigned long assetIndex=0;
    size_t bumpAmount=0;
    servableDataBlock_t block=new_data_block(determine_block_size(cur));
    block.assetCount=count_assets(cur);
    block.assets=calloc(block.assetCount, sizeof(servableAsset_t));
    cursor=block.data;
    while(cur) {
        bumpAmount=capture_str(cursor, cur->mimetype);
        block.assets[assetIndex].mimetype=cursor;
        cursor+=bumpAmount;

        bumpAmount=capture_str(cursor, cur->endpoint);
        block.assets[assetIndex].endpoint=cursor;
        cursor+=bumpAmount;

        bumpAmount=capture_file(cursor, cur->filePath);
        block.assets[assetIndex].content=cursor;
        block.assets[assetIndex].size=bumpAmount;
        cursor+=bumpAmount;

        cur=cur->next;
        ++assetIndex;
    }
    return block;
}

static servableDataBlock_t read_asset_files(assetListCell_t *head) {
    servableDataBlock_t block;
    block=initialize_block(head->next);
    cleanup_asset_list(head);
    return block;
}

servableDataBlock_t parse_asset_source(FILE *src) {
    buffer_t *inputBuffer=new_buffer(256);
    servableDataBlock_t block=read_asset_files(
        stream_in_config_info(src, inputBuffer));
    buffer_wipe(inputBuffer);
    return block;
}
