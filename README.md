# vomd web server

`vomd` is a web server for vomiting flat files over `HTTP`.

# Why?

Use `vomd` when you just want to get bytes to somebody somehow and all
better alternatives are unavailable to you.

If you want to simply copy files from one fully functional computer to
another, you should probably use [scp](http://www.computerhope.com/unix/scp.htm).

If you want a fully-featured, smart web server, you should probably
use [nginx](https://www.nginx.com/) or
[httpd](http://bsd.plumbing/about.html).

If you want to copy the electronic folk cover songs you recorded in
your basement onto your mom's Windows phone, `vomd` is well suited for
that task.

A regretable truth of computing today is that web browsers are found
on nearly every Turing Complete thing.  Having a quick-and-dirty web
server that doesn't require a bunch of setup or code to get running is
a convenient way to exchange data between computers.

# Quick Start

To install from source:

> `$ make && sudo make install`

To start the web server, run the following command:

> `$ vomd-auto-config <directory> | vomd -p <port>`

Where `<directory>` is the path containing files that you wish to
serve and `<port>` is the server's listening port number.

To stop the server, run the following command:

> `$ pkill vomd`

# Features

Here is a comprehensive list of all the features `vomd` supports:

 - HTTP `GET` requests
 - Easy setup
 - Large memory foot print (LOL)

## HTTP Requests

`vomd` will never support any HTTP request methods other than `GET`
because vomit only goes _one_ direction!

Setting up a server that can securely handle other request methods
would come at the cost of easy setup.

## Easy Setup

`vomd` aims at minimal setup.  If you're doing something that would
justify more sophisticated configurations, your use-case is far beyond
the scope of this project.

Configuration files for `vomd` are small, easy to edit or to
automatically generate with the `vomd-auto-config`.  The following
command will configure `vomd` to serve out all files found in the
`~/www` directory (and its subdirectories).

> `$ vomd-auto-config ~/www > ~/vomd.conf`

The following command will run the web server on port 8080:

> `$ vomd -p 8080 < ~/vomd.conf`

You may have noticed, `vomd` reads configs on `stdin`.  If you don't
care to keep your config file, you could simply redirect it to the web
server.

> `$ vomd-auto-config ~/www | vomd -p 8080`

If you don't specify a port, `vomd` assumes you want to run on
port 80.  Remember that port 80 is reserved for superuser so you'll
need to run this as root:

> `# vomd-auto-config ~/www | vomd`

## Memory Footprint

Yes, you read that corrcetly.  We're bragging about our large memory
footprint.

Because accessing the disk is slow and has some security implications,
`vomd` will preload all data into memory.  The amount of memory
consumed by `vomd` is the sum of the size of the files that it's
serving out plus some small overhead to record each file's metadata
some memory is also used to handle connections (naturally).

Once the server is listening for connections, it doesn't access the
disk, it doesn't fork, it doesn't run any other code.  This vastly
reduces the attack surface of the `vomd` server.

Avoiding disk access means we reduce the number of file descriptors
the server needs to serve out data and saves us time on expensive
IOPS.  This makes the likely performance bottlenecks network latency
and developer incompetence.

# Configuration Files

A configuration file can be made up of any number of lines.  Each line
represents a single servable asset in the web service and contains
three columns of text:

 1. File path
 2. Mimetype
 3. Endpoint

The file path column is a path of the file you wish to serve out.  The
mimetype column specifies which mimetype it is.  The endpoint column
contains the absolute path of the asset under the web server.

> `/tmp/mypage.html text/html /index.html`

The above config line will make the file found in `/tmp/mypage.html`
accessible.  The mimetype column will be transcribed into the
`Content-Type` header in responses.  The endpoint is the full path of
the asset as seen by web clients.

# Bugs And TODO's

`vomd` is currently in the alpha stages.

## Bugs

Bug reports can be filed at: [https://gitlab.com/ishpeck/vomd/issues](https://gitlab.com/ishpeck/vomd/issues)

## TODO's

The following are planned enhancements:

 - Improve performance of the socket layer code
 - Allow multiple endpoints to point to the same resource
 - Automatically generate directory indecies with `vomd-auto-config`

# Version Semantics

`vomd` versions will consist of the following components:

 - Development stage prefix
 - Release number
 - Build date

And will look something like this:

> a0-2016-05-31

## Development Stage Prefix

Development will occur in three stages:

 - Alpha
 - Beta
 - Stable

We are currently in the Alpha stage --- meaning that we believe this
software to be best suited for developers or enthusiasts only; we
don't know that the general public has any reason to care about this
project.  All alpha releases are prefixed with the letter `a`.

When project members believe (in our subjective opinion) that more
than just developers are likely to care about `vomd` we will move to
the Beta stage and begin prefixing releases with the letter `b`.

Once the project members believe that `vomd` is stable enough for
general use, we will change the prefix to the letter `v`.

## Release Number

At the beginning of each of the development stages, the first version
will be 0 (zero) and each subsequent version will increment this
release number by one, ad infinitum.  Because of the minimalistic
nature of `vomd`, we don't anticipate having a whole truckload of
release numbers --- maybe just a couple hundred.

Release numbers will increment after at least one bug or enhancement
has been implemented and tested to the satisfaction of project
participants.

## Bulid Date

Build date will follow the form of YYYY-MM-DD numeric dates and will
denote the day when the release was tested and declared satisfactory
to the community members.
