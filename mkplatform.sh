#!/bin/sh
uname | grep -e '^OpenBSD'
if [ "0" -eq "$?" ]; then
    exec sh -c "echo '#define PLATFORM_OPENBSD 1' > platform.h"
fi
echo '/* Your platform is insecure by design */' > platform.h
