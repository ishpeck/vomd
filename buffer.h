/* Copyright (C) 2016 Anthony "Ishpeck" Tedjamulia
   See LICENSE for licensing information. */
#ifndef _FUNKY_BUFFER_HEADER_FILE_
#define _FUNKY_BUFFER_HEADER_FILE_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define TST_MUST_VALIDATE_WITH_MEMMY_CHECK 1
#define TST_SHOULD_NOT_COMPILE_IF_BROKEN 1
#define TST_SHOULD_SEGFAULT_IF_BROKEN 1
#define TST_FAILURE 0
#define TST_SUCCESS 1

typedef struct {
    unsigned int len;
    unsigned int siz;
    unsigned int inc;
    char *data;
} buffer_t;

typedef struct {
    buffer_t *current;
    buffer_t *remaining;
} buffer_slice_t;

typedef int (*token_checker)(const char);

buffer_t *new_buffer(const unsigned int increment);
int buffer_push_char(buffer_t *buf, const char newChar);
int buffer_push_str(buffer_t *buf, const char *str);
buffer_t *buffer_init(const char *initialValue);
int buffer_wipe(buffer_t *buf);
int buffer_reset(buffer_t *buf);
int string_wipe(char *str);
char *buf_to_str(const buffer_t *buf);
char *write_and_destroy_buffer(buffer_t *buf);
char *str_compose(const char *templateStr);

buffer_t *buffer_sub(buffer_t *buf, const unsigned int start, const unsigned int len);
int buffer_contains(buffer_t *buf, const char *substr);

buffer_t *strip_leading(buffer_t *input, token_checker check);
buffer_slice_t split_buffer_on(buffer_t *buf, unsigned int splitpoint);
buffer_slice_t split_buffer_until(buffer_t *buf, token_checker check);
buffer_slice_t escaped_split_buffer_until(buffer_t *buf, token_checker check);
char trailing_char(buffer_t *buf);

buffer_slice_t empty_slice(void);
void wipe_slices(buffer_slice_t data);

buffer_t *stream_buffer(FILE *input);

#define buffer_length(x) x->len
#define buffer_size(x) x->siz
#define buffer_data(x) x->data

#endif
