/* Copyright (C) 2016 Anthony "Ishpeck" Tedjamulia
   See LICENSE for licensing information. */
#ifndef __HTTP_DETAILS_HEADERFILE__
#define __HTTP_DETAILS_HEADERFILE__

#include <string.h>
#include <time.h>

#include "assets.h"

#define RESPONSE_BUFFER_PADDING 60

#define HTTP_STATUS_OK 200
#define HTTP_STATUS_NOT_FOUND 404
#define HTTP_STATUS_REQUEST_TIMEOUT 408
#define HTTP_STATUS_REQUEST_TOO_LARGE 413
#define HTTP_STATUS_INTERNAL_ERROR 500
#define HTTP_STATUS_NOT_IMPLEMENTED 501

#define MAXIMUM_REQUEST_BODY_BYTES 2097152
#define MAXIMUM_REQUEST_BODY_WARN "2097152"

#define MAX_HTTP_METHOD_LENGTH 8
#define MAX_HTTP_VERSION_LENGTH 12

#define REMAIN_CONNECTED_AFTER_TRANSMISSION 0
#define DISCONNECT_AFTER_TRANSMISSION 1

typedef struct {
    char *responseBody;
    unsigned long responseLength;
    int disconnectAfterTx;
} http_response;

typedef http_response (*request_processing)(servableDataBlock_t *,
                                            const char *,
                                            const char *, int);

http_response process_http_request(servableDataBlock_t *src,
                                   const char *requestBody,
                                   unsigned int requestLength);

#endif
