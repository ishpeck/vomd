/* Copyright (C) 2016 Anthony "Ishpeck" Tedjamulia
   See LICENSE for licensing information. */
#include "server.h"

static void abort_due_to_socket_spawn_failure(void) {
    fprintf(stderr, "Unable to spawn socket!\n");
    exit(1);
}

static void abort_due_to_binding_failure(unsigned int port) {
    switch(errno) {
        case EACCES:
            fprintf(stderr, "No permission to bind port %d\n", port);
            break;
        case EADDRINUSE:
            fprintf(stderr, "Specified address already in use\n");
            break;
        case EBADF:
        case ENOTSOCK:
            fprintf(stderr, "Bad socket descriptor.\n");
            break;
        default:
            fprintf(stderr, "Couldn't bind port %d\n", port);
    }
    exit(2);
}

static void abort_due_to_select_failure(void) {
    switch(errno) {
        case EBADF:
            fprintf(stderr, "Couldn't select() because of bad FD\n");
            break;
        case EINVAL:
            fprintf(stderr, "Bad argument given to select().\n");
        default:
            fprintf(stderr, "Crashing to system due to select() failure: %d.\n", errno);
    }
    exit(3);
}

static int report_accept_failure(void) {
    fprintf(stderr, "Couldn't accept an incoming connection.\n");
    return 0;
}

static server_ctx *spawn_server_context(unsigned int port,
                                        unsigned int timeout,
                                        servableDataBlock_t block) {
    int optval=1;
    server_ctx *context=calloc(1, sizeof(server_ctx));
    context->listeningSocket=socket(AF_INET, SOCK_STREAM, 0); 
    setsockopt(context->listeningSocket, 
               SOL_SOCKET, 
               SO_REUSEADDR, 
               &optval, 
               sizeof(int));
    context->listeningPort=port;
    if(context->listeningSocket==-1)
        abort_due_to_socket_spawn_failure();
    context->svr.sin_addr.s_addr=INADDR_ANY;
    context->svr.sin_family=AF_INET;
    context->svr.sin_port=htons(port);
    context->backlogMax=3;
    FD_ZERO(&context->readableSockets);
    FD_ZERO(&context->writableSockets);
    context->data=block;
    context->client=NULL;
    context->clientTimeout=timeout;
    return context;
}

static void bind_to_port(server_ctx *context) {
    int bindResult=bind(context->listeningSocket,
                        (struct sockaddr *)&context->svr,
                        sizeof(context->svr));
    if(bindResult<0)
        abort_due_to_binding_failure(context->listeningPort);
    listen(context->listeningSocket, context->backlogMax);
}

static client_ctx *new_client(int newSocket, client_ctx *next) {
    client_ctx *newClient;
    newClient=calloc(1, sizeof(client_ctx));
    newClient->next=next;
    newClient->socket=newSocket;
    newClient->requestBody=new_buffer(1024);
    newClient->requestedAsset=-1;
    newClient->state=cs_requesting;
    newClient->response.responseBody=NULL;
    newClient->response.responseLength=0;
    newClient->opened=time(NULL);
    newClient->lastActive=newClient->opened;
    return newClient;
}

static int accept_incoming_client(server_ctx *context) {
    int newSocket;
    struct sockaddr_storage remoteAddress;
    socklen_t addressLength=sizeof(remoteAddress);
    newSocket=accept(context->listeningSocket,
                     (struct sockaddr *) &remoteAddress,
                     &addressLength);
    if(newSocket<0)
        return report_accept_failure();
    context->client=new_client(newSocket, context->client);
    return 1;
}

static int gracefully_close_client(client_ctx *client, int isError) {
    fprintf(stderr, "%s\n", (isError)
            ? "Error with client"
            : "Client disconnected");
    client->state=cs_closing;
    return 0;
}

static client_ctx *cleanup_client(client_ctx *cur) {
    client_ctx *next;
    if(!cur)
        return NULL;
    next=cur->next;
    buffer_wipe(cur->requestBody);
    close(cur->socket);
    memset(cur, 0, sizeof(client_ctx));
    free(cur);
    return next;
}

static client_ctx *cleanup_client_if_closed(server_ctx *server,
                                            client_ctx *client) {
    if(!client || client->state!=cs_closing || client->response.responseBody!=NULL)
        return client;
    FD_CLR(client->socket, &server->readableSockets);
    return cleanup_client(client);
}

static int drop_sluggish_client(client_ctx *client, time_t duration) {
    printf("We are silently dropping connections.  Duration %d "
           "It would be better if we were to send "
           "an HTTP response.\n", duration);
    client->state=cs_closing;
    return 0;
}

static int request_is_complete(buffer_t *requestBody) {
    return buffer_contains(requestBody, "\r\n\r\n");
}

static int request_not_done(buffer_t *req, time_t dur) {
    printf("Request not complete at %d bytes... \n"
           "======================\n"
           "%s"
           "\n===========\n"
           "Connection is %d seconds old\n",
           buffer_length(req) , buffer_data(req), dur);
    return 0;
}

static int process_client_request(server_ctx *context, client_ctx *client) {
    time_t now=time(NULL);
    time_t sessionDuration=now-client->opened;
    client->lastActive=now;
    
    if(sessionDuration>MAXIMUM_SESSION_DURATION_SECONDS)
        return drop_sluggish_client(client, sessionDuration);
    if(!request_is_complete(client->requestBody))
        return request_not_done(client->requestBody, sessionDuration);
    client->response=process_http_request(&context->data,
                                          buffer_data(client->requestBody),
                                          buffer_length(client->requestBody));
    client->state=cs_listening;
    buffer_reset(client->requestBody);
    return 1;
}

static int read_request_from_client(server_ctx *context, client_ctx *client) {
    int i;
    int bytesReceived;
    char buffer[512];
    bytesReceived = recv(client->socket, buffer, sizeof(buffer), 0);
    if(bytesReceived<1)
        return gracefully_close_client(client, bytesReceived!=0);
    for(i=0;i<=bytesReceived;++i)
        buffer_push_char(client->requestBody, buffer[i]);
    return process_client_request(context, client);
}

static int report_write_failure(void) {
    printf("Couldn't transmit to client.\n");
    return 0;
}

static int write_response_for_client(server_ctx *context, client_ctx *client) {
    if(send(client->socket, 
            client->response.responseBody,
            client->response.responseLength, 0)<0)
        return report_write_failure();

    client->state=(client->response.disconnectAfterTx) 
        ? cs_closing : cs_requesting;

    free(client->response.responseBody);
    client->response.responseLength=0;
    client->response.responseBody=NULL;
    client->response.disconnectAfterTx=DISCONNECT_AFTER_TRANSMISSION;

    return 1;
}

static int topFileDescriptor(server_ctx *context) {
    int topFD=context->listeningSocket;
    client_ctx *cur=context->client;
    while(cur) {
        if(topFD < cur->socket)
            topFD = cur->socket;
        cur=cur->next;
    }
    return topFD+1;
}

static client_ctx *handle_client(server_ctx *context, client_ctx *curClient) {
    if(!curClient)
        return curClient;
    switch(curClient->state) {
        case cs_requesting:
            if(FD_ISSET(curClient->socket, &context->readableSockets))
                read_request_from_client(context, curClient);
            break;
        case cs_listening:
            if(FD_ISSET(curClient->socket, &context->writableSockets))
                write_response_for_client(context, curClient);
            break;
        case cs_closing:
            printf("\n\nClosed connection!\n");
        default:
            fprintf(stderr, "Client state special\n");
    }
    curClient->next=cleanup_client_if_closed(context, curClient->next);
    return curClient->next;
}

static int prep_sockets_and_client_state(server_ctx *context) {
    client_ctx *cur;
    time_t now=time(NULL);
    FD_ZERO(&context->readableSockets);
    FD_ZERO(&context->writableSockets);
    FD_SET(context->listeningSocket, &context->readableSockets);
    for(cur=context->client;cur!=NULL;cur=cur->next) {
        switch(cur->state) {
            case cs_requesting:
                FD_SET(cur->socket, &context->readableSockets);
                break;
            case cs_listening:
                FD_SET(cur->socket, &context->writableSockets);
                break;
            default:
                FD_CLR(cur->socket, &context->writableSockets);
                FD_CLR(cur->socket, &context->readableSockets);
        }
        if(now-cur->lastActive>context->clientTimeout)
            cur->state=cs_closing;
    }
    return 0;
}

static int process_connected_clients(server_ctx *context, client_ctx *head) {
    client_ctx *cur;
    for(cur=head;cur!=NULL;cur=handle_client(context, cur));
    return 0;
}

static int server_main_loop(server_ctx *context) {
    struct timeval *timeout=calloc(1, sizeof(struct timeval));
    timeout->tv_sec=20;
    timeout->tv_usec=0;
    prep_sockets_and_client_state(context);
    if(select(topFileDescriptor(context),
              &context->readableSockets, 
              &context->writableSockets, NULL, timeout)<0)
        abort_due_to_select_failure();
    if(FD_ISSET(context->listeningSocket, &context->readableSockets))
        accept_incoming_client(context);
    process_connected_clients(context, context->client);
    context->client=cleanup_client_if_closed(context, context->client);
    free(timeout);
    return 1;
}

static int run_server(server_ctx *context) {
    bind_to_port(context);
    printf("Listening on port %d.\n", context->listeningPort);
    printf("Serving %d asset%s of %d total bytes.\n",
           context->data.assetCount,
           (context->data.assetCount == 1) ? "" : "s",
           context->data.size);
    while(server_main_loop(context));
    return 0;
}

static int cleanup_server(server_ctx *context) {
    close(context->listeningSocket);
    while(context->client=cleanup_client(context->client));
    free(context);
    destroy_data_block(context->data);
    return 0;
}

int server(unsigned int listeningPort, 
           unsigned int clientTimeout, 
           servableDataBlock_t dataToServe) {
    server_ctx *context=spawn_server_context(listeningPort, 
                                             clientTimeout,
                                             dataToServe);
    run_server(context);
    cleanup_server(context);
    return 0;
}
