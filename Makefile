vomd: assets.c assets.h config.c config.h http.c http.h main.c server.c server.h vomd.h buffer.c buffer.h platform.h Makefile server_continues.h
	gcc -o vomd main.c assets.c buffer.c config.c server.c http.c

server_continues.h:
	echo '#define CONTINUE_RUNNING 1' > server_continues.h

platform.h:
	./mkplatform.sh

run: vomd
	cat configgy | ./vomd -p 8080

install: vomd
	install vomd /usr/local/bin/
	install vomd-auto-config /usr/local/bin/

server: vomd

assets: assets.c assets_test.c buffer.c config.c config.h assets.h buffer.h http.h http.c
	gcc -o assets assets.c config.c assets_test.c buffer.c http.c

asset_tests: assets
	cat configgy | ./assets 

test:
	curl -si http://127.0.0.1:8080/index.html | sed -e 's/Date: \([a-zA-Z ,0-9:-]*\)/Date: IGNORED/' > /tmp/actual_index.html
	diff /tmp/actual_index.html ./expectations/index.html
	curl -si http://127.0.0.1:8080/README.html | sed -e 's/Date: \([a-zA-Z ,0-9:-]*\)/Date: IGNORED/' > /tmp/actual_README.html
	diff /tmp/actual_README.html ./expectations/README.html
	curl -H 'Connection: Keep-Alive' -si http://127.0.0.1:8080/server.c > /dev/null
	curl -X PUT -si http://127.0.0.1:8080/whatever | sed -e 's/Date: \([a-zA-Z ,0-9:-]*\)/Date: IGNORED/' > /tmp/actual_put
	diff /tmp/actual_put ./expectations/put
	curl -X POST -si http://127.0.0.1:8080/whatever | sed -e 's/Date: \([a-zA-Z ,0-9:-]*\)/Date: IGNORED/' > /tmp/actual_post
	diff /tmp/actual_post ./expectations/post
	curl -X DELETE -si http://127.0.0.1:8080/whatever | sed -e 's/Date: \([a-zA-Z ,0-9:-]*\)/Date: IGNORED/' > /tmp/actual_delete
	diff /tmp/actual_delete ./expectations/delete

server_aborts:
	echo '#define CONTINUE_RUNNING 0' > server_continues.h

memcheck: server_aborts vomd
	mv vomd memcheck
	rm server_continues.h
