/* Copyright (C) 2016 Anthony "Ishpeck" Tedjamulia
   See LICENSE for licensing information. */
#include "buffer.h"

static void obliterate_heap_string(char *heapStr, size_t siz) {
    if(!heapStr)
        return;
    memset(heapStr, 0, siz);
    free(heapStr);
}

static void obliterate_heap_buffer(buffer_t *heapBuf) {
    if(!heapBuf)
        return;
    memset(heapBuf, 0, sizeof(heapBuf));
    free(heapBuf);
}

static void abort_due_to_calloc_failure(void) {
    fprintf(stderr, "Could not allocate memory for new string buffer\n");
    exit(1);
}

char *new_strbuf(const unsigned int length) {
    return calloc(length, sizeof(char));
}

buffer_t *new_buffer(unsigned int increment) {
    buffer_t *newBuffer=calloc(1, sizeof(buffer_t));
    if(!newBuffer)
        abort_due_to_calloc_failure();
    newBuffer->inc=increment;
    newBuffer->len=0;
    newBuffer->siz=newBuffer->inc;
    newBuffer->data=new_strbuf(newBuffer->siz);
    return newBuffer;
}

static int buffer_plan_resize(buffer_t *buf, unsigned int increment) {
    int resizeRequired=0;
    while(buf->len+increment>=buf->siz) {
        resizeRequired=1;
        buf->siz+=buf->inc;
    }
    return resizeRequired;
}

static int buffer_grow(buffer_t *buf, unsigned int increment) {
    char *tmp;
    size_t oldSize=buf->siz;
    if(!buffer_plan_resize(buf, increment))
        return 1;
    tmp=new_strbuf(buf->siz);
    if(buf->len>0)
        snprintf(tmp, buf->siz, "%s", buf->data);
    obliterate_heap_string(buf->data, oldSize);
    buf->data=tmp;
    return 0;
}

int buffer_push_char(buffer_t *buf, const char newChar) {
    buffer_grow(buf, buf->inc);
    buf->data[buf->len++]=newChar;
    return 0;
}

int buffer_push_str(buffer_t *buf, const char *str) {
    char *addable=(char *)str;
    if(!str || !*str) 
        return 1;
    buffer_grow(buf, strlen(str));
    while(*addable) {
        buffer_push_char(buf, *addable);
        addable++;
    }
    return 0;
}

buffer_t *buffer_init(const char *initialValue) {
    buffer_t *newBuffer=new_buffer(strlen(initialValue));
    buffer_push_str(newBuffer, initialValue);
    return newBuffer;
}

int buffer_reset(buffer_t *buf) {
    if(!buf || !buf->data) return 1;
    memset(buf->data, 0, buf->siz);
    buf->len=0;
    return 0;
}

int buffer_wipe(buffer_t *buf) {
    if(!buf)
        return 1;
    obliterate_heap_string(buf->data, buf->siz);
    obliterate_heap_buffer(buf);
    return 0;
}

char *str_compose(const char *source) {
    int len = strlen(source)+1;
    char *output = calloc(len, sizeof(char));
    snprintf(output, len, "%s", source);
    return output;
}

char *buf_to_str(const buffer_t *buf) {
    return str_compose(buffer_data(buf));
}

char *write_and_destroy_buffer(buffer_t *buf) {
    char *out=buf_to_str(buf);
    buffer_wipe(buf);
    return out;
}

static buffer_t *extract_subbuffer(buffer_t *buf, unsigned int start, unsigned int len) {
    buffer_t *sub=new_buffer(len+1);
    char *i=buffer_data(buf);
    for(;start>0;--start)
        ++i;
    for(;len>0;--len)
        buffer_push_char(sub, *i++);
    return sub;
}

buffer_t *buffer_sub(buffer_t *buf, unsigned int start, unsigned int len) {
    if(start>=buffer_length(buf))
        return NULL;
    if(start+len>buffer_length(buf))
        len=buffer_length(buf)-start;
    return extract_subbuffer(buf, start, len);
}

int buffer_contains(buffer_t *buf, const char *substr) {
    return strstr(buffer_data(buf), substr)!=NULL;
}

buffer_t *strip_leading(buffer_t *input, token_checker check) {
    buffer_t *tmp;
    char *in=buffer_data(input);
    unsigned int max=1;
    while(check(*in)) {
        ++in;
        ++max;
    }
    tmp=new_buffer(max);
    buffer_push_str(tmp, in);
    return tmp;
}

char trailing_char(buffer_t *buf) {
    if(buf->len<1)
        return 0;
    return buffer_data(buf)[buf->len-1];
}

buffer_slice_t empty_slice(void) {
    buffer_slice_t data;
    data.current=NULL;
    data.remaining=NULL;
    return data;
}

void wipe_slices(buffer_slice_t data) {
    buffer_wipe(data.current);
    buffer_wipe(data.remaining);
}

buffer_t *stream_buffer(FILE *input) {
    buffer_t *buf=new_buffer(64);
    int inchar;
    while((inchar=getc(input))!=EOF)
        buffer_push_char(buf, (char)inchar);
    return buf;
}
