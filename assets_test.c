/* Copyright (C) 2016 Anthony "Ishpeck" Tedjamulia
   See LICENSE for licensing information. */
#include "assets.h"
#include "config.h"

static void display_asset(servableAsset_t *asset, const char *wanted) {
    int i;
    if(!asset) {
        printf("%s wasn't found.\n", wanted);
        return;
    }
    printf("Endpoint: %s\n", asset->endpoint);
    printf("Mimetype: %s\n", asset->mimetype);
    for(i=0;i<asset->size;++i)
        putchar(*(char *)(asset->content+i));
}

static void test_asset_lookup(servableDataBlock_t *src, const char *wanted) {
    display_asset(lookup_asset(src, wanted), wanted);
}

int main(void) {
    int i;
    servableDataBlock_t servableData=parse_asset_source(stdin);
    for(i=0;i<servableData.assetCount;++i)
        test_asset_lookup(&servableData, servableData.assets[i].endpoint);
    test_asset_lookup(&servableData, "/doesn'texist");
    return 0;
}
