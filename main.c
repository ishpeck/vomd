/* Copyright (C) 2016 Anthony "Ishpeck" Tedjamulia
   See LICENSE for licensing information. */
#include "vomd.h"

static unsigned int text_to_whole_num(const char *textualNum) {
#ifndef PLATFORM_OPENBSD
    return (unsigned int)strtoul(textualNum, NULL, 10);
#else
    return (unsigned int)strtonum(textualNum, 1, 65535, NULL);
#endif
}

static unsigned int locate_CL_arg(int argc, char **argv, const char *arg) {
    int i;
    for(i=0;i<argc;++i)
        if(strcmp(argv[i], arg)==0 && ++i<argc)
            return i;
    return NO_MORE_ARGS;
}

static unsigned int parse_port_CL_arg(int argc, char **argv) {
    int i=locate_CL_arg(argc, argv, "-p");
    if(i>0 && i<argc) {
        unsigned int parsedNum=text_to_whole_num(argv[i]);
        if(parsedNum!=0)
            return parsedNum;
        fprintf(stderr, "%d not a valid port number\n", parsedNum);
        exit(1);
    }
    return DEFAULT_LISTENING_PORT;
}

static FILE *parse_config_stream(int argc, char **argv) {
    int i=locate_CL_arg(argc, argv, "-f");
    struct stat statInfo;
    if(i>0 && i<argc)
        if(stat(argv[i], &statInfo)>=0)
            return fopen(argv[i], "r");
    return stdin;
}

static unsigned int parse_client_timeout(int argc, char **argv) {
    int i=locate_CL_arg(argc, argv, "-t");
    if(i>0 && i<argc) {
        unsigned int parsedNum=text_to_whole_num(argv[i]);
        if(parsedNum>=5 && parsedNum<=300)
            return parsedNum;
        fprintf(stderr,
                "%d not a valid timeout number.  "
                "Try a number from 5 to 300\n", 
                parsedNum);
        exit(1);
    }
    return DEFAULT_REQUEST_DURATION_SECONDS;
}

static int switch_given(int argc, char **argv, const char *wantedSwitch) {
    int i;
    for(i=0;i<argc;++i) 
        if(strcmp(wantedSwitch, argv[i])==0)
            return 1;
    return 0;
}

static int nothing_to_do(void) {
    printf("Nothing to do.  No assets were specified.\n"
           "Try running ``vomd-auto-config <path> | vomd'' to serve out all "
           "files in <path>.\n");
    return 0;
}

static int init_server(unsigned int port,
                       unsigned int timeout,
                       servableDataBlock_t assetInfo) {
    if(assetInfo.assetCount>0)
        return server(port, timeout, assetInfo);
    return nothing_to_do();
}

static int version_or_help_switch_given(int argc, char **argv) {
    return switch_given(argc, argv, "-h") || switch_given(argc, argv, "-v");
}

static int show_usage(const char *programName) {
    printf("%s version %s\nUsage: %s [-hv] [-p port] [-f file] [-t timeout]\n", 
           programName,
           VOMD_VERSION_IDENTIFIER,
           programName);
    return 0;
}

int main(int argc, char **argv) {
    int returnCode;
    if(version_or_help_switch_given(argc, argv))
        return show_usage(basename(argv[0]));
    returnCode=init_server(parse_port_CL_arg(argc, argv),
                           parse_client_timeout(argc, argv),
                           parse_asset_source(parse_config_stream(argc, argv)));
    fclose(stdout);
    fclose(stderr);
    fclose(stdin);
    return returnCode;
}
