/* Copyright (C) 2016 Anthony "Ishpeck" Tedjamulia
   See LICENSE for licensing information. */
#ifndef _YON_ASSETS_HEADER_
#define _YON_ASSETS_HEADER_

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#define MAXIMUM_ENDPOINT_LENGTH 2048
#define MAX_ENDPOINT_BUFFER_LENGTH 2050

typedef struct {
    char *mimetype;
    char *endpoint;
    void *content;
    off_t size;
} servableAsset_t;

typedef struct {
    servableAsset_t *assets;
    unsigned long assetCount;
    void *data;
    off_t size;
} servableDataBlock_t;

#define RIGHT_ASSET_COMES_AFTER_LEFT(x) x<0

servableDataBlock_t new_data_block(off_t totalSize);
servableAsset_t *lookup_asset(servableDataBlock_t *data, const char *endpoint);
void destroy_data_block(servableDataBlock_t destroyMe);

#endif
