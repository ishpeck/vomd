/* Copyright (C) 2016 Anthony "Ishpeck" Tedjamulia
   See LICENSE for licensing information. */
#ifndef _WEB_SERVER_HEADER_
#define _WEB_SERVER_HEADER_

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/select.h>

#include "buffer.h"
#include "assets.h"
#include "http.h"

#define SERVER_RUNNING 1

#define DEFAULT_REQUEST_DURATION_SECONDS 15
#define MAXIMUM_SESSION_DURATION_SECONDS 30

typedef enum {
    cs_idle,
    cs_requesting,
    cs_listening,
    cs_closing
} client_state;

typedef struct {
    int socket;
    time_t opened;
    time_t lastActive;
    buffer_t *requestBody;
    long requestedAsset;
    client_state state;
    http_response response;
    void *next;
} client_ctx;

typedef struct {
    int listeningSocket;
    int listeningPort;
    int backlogMax;
    struct sockaddr_in svr;
    fd_set readableSockets;
    fd_set writableSockets;
    servableDataBlock_t data;
    client_ctx *client;
    unsigned int clientTimeout;
} server_ctx;

int server(unsigned int listeningPort,
           unsigned int clientTimeout,
           servableDataBlock_t dataToServe);

#endif
